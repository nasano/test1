import json, pprint, copy, os


def build_models(d):
    user_model = {}
    tags_model = {}
    friends_model = {}
    for key, value in d.items():
        if not isinstance(value, list):
            user_model[key] = None
        else:
            if key == "tags":
                tags_model[key] = None
            if key == "friends":
                friends_model[key] = None
    return user_model, tags_model, friends_model


def get_data():
    """Допустим тут приходят данные с сервера."""

    with open('res.json', 'r') as f:
        res = json.load(f)

    return res


def validate(item):
    if isinstance(item, list):
        item = json.dumps(item)
    if isinstance(item, dict):
        item = json.dumps(item)
    item = str(item).replace("\n", "").replace("\r", "")

    return item


def save_data(data, filename):
    with open(filename, "a+", encoding="utf8") as f:
        f.seek(0, os.SEEK_SET)
        headers = f.readline()
        f.seek(0, os.SEEK_END)
        if headers:
            f.write(";".join(data.values()) + "\n")
        else:
            f.write(";".join(data.keys()) + "\n")
            f.write(";".join(data.values()) + "\n")


def build_tables():
    """А тут надо бы их обработать."""
    pass


def main():
    res = get_data()
    for i in res:
        user_model, tags_model, friends_model = build_models(i)
        user = copy.deepcopy(user_model)
        tags = copy.deepcopy(tags_model)
        friends = copy.deepcopy(friends_model)
        for j in user:
            user[j] = validate(i[j])
        for j in tags:
            tags[j] = validate(i[j])
        tags["_id"] = user["_id"]

        for j in friends:
            friends[j] = validate(i[j])
        friends["_id"] = user["_id"]

        save_data(user, "user.csv")
        save_data(tags, "tags.csv")
        save_data(friends, "friends.csv")


if __name__ == '__main__':
    main()
