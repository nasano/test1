import json
import os


def open_file(file_name):
    with open(file_name, 'r') as f:
        file = json.load(f)
    return file


def filter_data(data):
    user = data
    tag, friend = {}, {}
    tag['_id'] = user['_id']
    tag['tags'] = user['tags']
    friend['_id'] = user['_id']
    friend['friends'] = user['friends']
    del user['tags']
    del user['friends']
    return user, tag, friend


def save_file(file_name, data):
    with open(file_name, 'a+', encoding="utf8") as f:
        f.seek(0, os.SEEK_SET)
        not_empty_file = f.readline()
        f.seek(0, os.SEEK_END)
        if not_empty_file:
            for value in data.values():
                f.write(str(value).replace("\n", "").replace("\r", "") + ';')
            f.write('\n')
        else:
            for key in data.keys():
                f.write(str(key).replace("\n", "").replace("\r", "") + ';')
            f.write('\n')
            for value in data.values():
                f.write(str(value).replace("\n", "").replace("\r", "") + ';')
            f.write('\n')


def main():
    opened_file = open_file('res.json')
    for i in range(len(opened_file)):
        user, tag, friend = filter_data(opened_file[i])
        save_file('alternative_user.csv', user)
        save_file('alternative_tag.csv', tag)
        save_file('alternative_friend.csv', friend)


if __name__ == '__main__':
    main()
